//
//  Rocket.swift
//  FalconSwift
//
//  Created by Leszek Kaczor on 12/04/2018.
//  Copyright © 2018 Blaze. All rights reserved.
//

struct Rocket {
    var name: String
    var photoUrl: String
}
