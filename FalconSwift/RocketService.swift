//
//  RocketService.swift
//  FalconSwift
//
//  Created by Leszek Kaczor on 12/04/2018.
//  Copyright © 2018 Blaze. All rights reserved.
//

import RxSwift

class RocketService {
    
    private let rockets: [Rocket] = [
        Rocket(name: "Falcon 1", photoUrl: "http://www.spacex.com/files//assets/img/Liftoff_south_FULL_QQ9L7636-480x723.jpg"),
        Rocket(name: "Falcon 1e", photoUrl: "http://michaelgr.wordpress.com/files/2008/09/spacex-falcon1.jpg"),
        Rocket(name: "Falcon 9 v1.0", photoUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/SpX_CRS-2_launch_-_further_-_cropped.jpg/954px-SpX_CRS-2_launch_-_further_-_cropped.jpg"),
        Rocket(name: "Falcon 9 v1.1", photoUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Falcon_9_launch_with_DSCOVR.jpg/1024px-Falcon_9_launch_with_DSCOVR.jpg"),
        Rocket(name: "Falcon 9 Full Thrust", photoUrl: "https://upload.wikimedia.org/wikipedia/commons/6/60/ORBCOMM-2_%2823802549782%29.jpg"),
        Rocket(name: "Falcon Heavy", photoUrl: "https://i.wpimg.pl/O/593x450/d.wpimg.pl/525385494--961962968/spacex.png"),
        Rocket(name: "BFR", photoUrl: "https://upload.wikimedia.org/wikipedia/en/9/90/SpaceX_BFR_launch_vehicle.jpg")
    ]
    
    func fetchRocketList() -> Observable<[Rocket]> {
        return Observable
            .just(rockets)
            .delaySubscription(RxTimeInterval(2), scheduler: MainScheduler.instance)
    }
}
